//
//  ViewControllerSignIn.swift
//  EasyRun
//
//  Created by 247 on 24/04/19.
//  Copyright © 2019 247. All rights reserved.
//

import UIKit

class ViewControllerSignIn: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordtextField: UITextField!
    
    @IBAction func buttonSignIn(_ sender: Any) {
        let email = emailTextField.text!
        let password = passwordtextField.text!
        
        if email.isEmpty || password.isEmpty{
            let alert = UIAlertController(title: "Upss!", message: "Email / Password wajib diisi!", preferredStyle: .alert)
            let aksi = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(aksi)
            present(alert, animated: true, completion: nil)
        }
        if email=="titah@titah.com" && password=="1234"{
            //
        }else{
            let alert = UIAlertController(title: "Gagal!", message: "Email / Password anda salah!", preferredStyle: .alert)
            let aksi = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(aksi)
            present(alert, animated: true, completion: nil)
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
