//
//  ViewControllerActivity.swift
//  EasyRun
//
//  Created by 247 on 22/04/19.
//  Copyright © 2019 247. All rights reserved.
//

import UIKit
import Charts

class ViewControllerActivity: UIViewController {
    
    @IBOutlet weak var lineChart: LineChartView!
    @IBOutlet weak var barChart: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCurrentDate()
        
        let days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        //let barValues = Int(arc4random_uniform(UInt32(5))+3)
        
        setLineChart(dataPoints: days)
        setBarChart(dataPoints: days)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "ico_date")
        imageView.image = image
        
        navigationItem.titleView = imageView
        
    }
    
    func getCurrentDate() {
        let dateLabel = UILabel(frame: CGRect(x: 21, y: 0, width: 40, height: 20))
        dateLabel.contentMode = .scaleAspectFit
        let label1 = UILabel()
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        let date1 = formatter.string(from: Date())
        label1.text = date1
        
        navigationItem.titleView = dateLabel
    }
    
    func setLineChart(_count : Int = 7, dataPoints: [String]){
        let values = (0..<_count).map {(i) -> ChartDataEntry in
            let val = Double(arc4random_uniform(UInt32(_count))+3)
            return ChartDataEntry(x: Double(i), y: val)
        }
        
        let line1 = LineChartDataSet(entries: values, label: "Distance (Km) ")
        let data = LineChartData(dataSet: line1)
        
        line1.colors = [NSUIColor.blue]
        line1.circleRadius = 4.0
        line1.mode = .cubicBezier
        line1.cubicIntensity = 0.2
        
        let gradient = getGradientFilling()
        line1.fill = Fill.fillWithLinearGradient(gradient, angle: 90.0)
        line1.drawFilledEnabled = true
        
        data.addDataSet(line1)
        lineChart.data = data
        lineChart.setScaleEnabled(false)
        lineChart.animate(xAxisDuration: 2.0)
        lineChart.drawGridBackgroundEnabled = true
        lineChart.xAxis.drawAxisLineEnabled = true
        lineChart.xAxis.drawGridLinesEnabled = true
        lineChart.leftAxis.drawGridLinesEnabled = true
        lineChart.leftAxis.drawAxisLineEnabled = true
        lineChart.rightAxis.drawAxisLineEnabled = false
        lineChart.rightAxis.drawGridLinesEnabled = false
        //lineChart.legend.enabled = true
        lineChart.xAxis.enabled = false
        lineChart.leftAxis.enabled = false
        lineChart.rightAxis.enabled = false
        lineChart.xAxis.drawLabelsEnabled = true
        
    }
    
    private func getGradientFilling() -> CGGradient{
        let colorTop = UIColor(red: 141/255, green: 133/255, blue: 220/255, alpha: 1).cgColor
        let colorBottom = UIColor(red: 230/255, green: 155/255, blue: 155/255, alpha: 1).cgColor
        let gradientColors = [colorTop,colorBottom] as CFArray
        let colorLocations: [CGFloat] = [0.7,0.0]
        
        return CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)!
    }
    
    func setBarChart(_count : Int = 7, dataPoints: [String]) {
        let values = (0..<_count).map {(i) -> ChartDataEntry in
            let val = Double(arc4random_uniform(UInt32(_count))+3)
            return BarChartDataEntry(x: Double(i), y: val)
        }
        let bar1 = BarChartDataSet(entries: values, label: "Duration (Hours)")
        let data = BarChartData(dataSet: bar1)
        //bar1.colors = ChartColorTemplates.colorful()
        bar1.colors = [UIColor.orange]
        data.addDataSet(bar1)
        barChart.data = data
        barChart.setScaleEnabled(false)
        barChart.drawGridBackgroundEnabled = true
        barChart.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        barChart.xAxis.enabled = false
        barChart.leftAxis.enabled = false
        barChart.rightAxis.enabled = false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
